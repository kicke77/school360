<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected $layout = 'master';
	public    $title = '';
	
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			//$this->layout->title = "NetSchool360 Dev 0.2";
			$this->layout = View::make($this->layout);
		}
	}
	protected function logAccesos($modulo, $motivo, $description){
				Session::keep(array('key', 'email'));
			    $userLog = Session::get('key');
			    $logg = new SysLog();
			    $dt = new DateTime('now');
			    //$start_day = date("Y-m-d", strtotime($date));
			    $logg->fecha = "2050-11-11 16:01:38";
			    $logg->id_user = 1;
			    $logg->ip = $_SERVER['REMOTE_ADDR'];
			    $logg->motivo = $motivo;
			    $logg->module =$modulo;
			   	$logg->description= $description;
			   	$logg->save();
	}


}
