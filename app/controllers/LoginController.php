<?php
class LoginController extends Controller
{
 
    //establecemos restful a true
    public $restful = true;
 
    //al hacer uso de get le decimos a laravel que queremos crear una ruta,
    //cargar una vista etc
    public function get_index()
    {
 
        //si se ha iniciado sesión no dejamos volver
        if(Auth::user())
        {
            return Redirect::to('home');
        }
        //mostramos la vista views/login/index.blade.php pasando un título
        return View::make('login')->with('title','Login');
 
    }
 
    //anteponemos post al nombre de la función, esto es así porque es la función
    //que recibirá datos por post
    public function post_index()
    {
 
        //recogemos los campos del formulario y los guardamos en un array
        //para pasarselo al método Auth::attempt
        $userdata = array(
 
            'username' => Input::get('username'),
            'password'=> Input::get('password')
 
        );
 
        //si los datos son correctos y existe un usuario con los mismos se inicia sesión
        //y redirigimos a la home
        if(Auth::attempt($userdata, true))
        {
 
            return Redirect::to('home');
 
        }else{
            //en caso contrario mostramos un error
            return Redirect::to('login')->with('error_login', true);
 
        }
    }
    public function index(){
        return View::make('login');
    }
    public function login(){

    }
    public function getLogin(){

        $testuser= User::where('email','=', 'cstan.77@outlook.com' );
        foreach ($testuser as $key => $value) {
            var_dump($value);
        }
        return View::make('login');
    }
    public function postLogin(){
        $valid = Validator::make(Input::all(),
            array(
                'username'=>'required|email',
                'password'=>'required'
            )
        );
        if(!$valid->fails()){
                
            $user = User::where('email','=', Input::get('username') );
            if($user->count()){
                $user = $user->first();
                if(Hash::check(Input::get('password'), $user->password)){   
                    if ($user->active == 1) {
                        Auth::login($user);
                        if($user->password_temp != ''){
                            return Redirect::route()->with('warning',true);
                        }
                        //print_r(Input::all());
                        //die('everything is OK!');
                        return Redirect::intended('/dashboard');
                    }
                    return Redirect::route('login')->withInput()->with('error','inactive-account');
                }else{
                    return Redirect::route('login')->withInput()->with('error','invalid-account');
                }
            }
            return Redirect::route('login')->withInput()->with('error','account-doesnt-exits');
        }
        return Redirect::route('login')->withInput()->with('error','invalid-account');
    }
 
}