<?php 
class MasterTableController extends TemplateSchoolController{

	public function lista(){

		$mastertbl = Mastertbl::all();

		$title = "Maestro de Tablas";
		View::share('title',$title);
		return $this->layout->content = View::make('MasterTable.list')->with('mastertbl',$mastertbl);
	}
	public function newtbl(){
		$title = "Maestro de Tablas";
		View::share('title',$title);
		return $this->layout->content =  View::make('MasterTable.new');
	}
	public function savetbl(){

		$valid = Validator::make(Input::all(),array(
			'titulo'=> 'required',
			'descripcion'=> 'required',
			'funcion'=> 'required',
			));
		if($valid->passes()){
			$user = Auth::user();
			$mastertbl = new Mastertbl();
			$mastertbl->tx_titulo = Input::get('titulo');
			$mastertbl->tx_descripcion = Input::get('descripcion');
			$mastertbl->tx_function = Input::get('titulo');
			$mastertbl->est = "1";
			$mastertbl->save();
			if($mastertbl->save()) {
					return Redirect::route('mastertable');
				}
		}
		return Redirect::route('mastertable-new')->withErrors($valid);
	}
	public function edittbl($id){
		$title = "Maestro de Tablas";
		View::share('title',$title);
		$mastertbl= Mastertbl::find($id);
		return $this->layout->content = View::make('MasterTable.edit')->with('mastertbl',$mastertbl);
	}
	public function updatetbl($id){
		$valid = Validator::make(Input::all(),array(
			'titulo'=> 'required',
			'descripcion'=> 'required',
			'funcion'=> 'required',
			));
			if($valid->passes()){
			$user = Auth::user();
			$mastertbl= Mastertbl::find(Input::get('id'));
			$mastertbl->tx_titulo = Input::get('titulo');
			$mastertbl->tx_descripcion = Input::get('descripcion');
			$mastertbl->tx_function = Input::get('titulo');
			$mastertbl->save();
			if($mastertbl->save()) {
					return Redirect::route('mastertable');
				}
		}
		return Redirect::route('mastertable-edit')->withErrors($valid);
	}
	public function dardealta($param){
          	$mastertbl= Mastertbl::find($param);
			$mastertbl->est = "1";
			$mastertbl->save();
			if($mastertbl->save()) {
					return Redirect::route('mastertable');
				}
	}
	public function dardebaja($param){
			$mastertbl= Mastertbl::find($param);
			$mastertbl->est = "0";
			$mastertbl->save();
			if($mastertbl->save()) {
					return Redirect::route('mastertable');
				}
	}

}


 ?>