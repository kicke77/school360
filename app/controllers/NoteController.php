<?php 
class NoteController extends BaseController{
	public function index(){
		$jsfiles = array('');
		$title = 'Notas';
	    View::share('title', $title);
		return View::make('note.index')->with('jsfiles', $jsfiles);
	}
}
?>