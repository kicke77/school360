<?php
class PersonaController extends BaseController {

        public function lista(){
                $persona = Persona::all();  
                $View= View::make('persona.index')->with('personas',$persona);  
        }
        public function show($id) {
        $user = User::find($id);
        return View::make('persona.show')->with('persona', $persona);
    }
	    public function index() {
        //$personas = Persona::all();
            $personas = Persona::take(10)->skip(0)->OrderBy('id_person','DESC')->get();
        //$personAll= Persona::Pagination(10);
        return View::make('persona.index')->with('personas', $personas);
    }
        public function create() {
        $persona = new Persona();
        return View::make('persona.save')->with('persona', $persona);
    }
    public function store() {

        //validacion
        $rules = array(
            'name'=> 'required',
            'apepat'=> 'required',
            'apemat'=> 'required',
            'address'=> 'required',
            'telephone'=> 'required|numeric',
            'mobile'=> 'required|numeric'
        );
        $valid = Validator::make(Input::all(), $rules);
        if($valid->fails()){
            return Redirect::to('personas/create')
                ->withErrors($valid);
        }else{
        $persona = new Persona();
        $teacher= new Teacher();
        $persona->name = Input::get('name');
        $persona->apepat = Input::get('apepat');
        $persona->apemat = Input::get('apemat');
        $persona->address = Input::get('address');
        $persona->telephone = Input::get('telephone');
        $persona->mobile = Input::get('mobile');
        $persona->save();
        $persona->teacher()->save($teacher);
        Session::flash('message', 'Profesor creador exitosamente!');
        return Redirect::to('personas')
                ->with('notice', 'El Profesor ha sido creado correctamente.');

        }        
    }
    public function edit($id)
    {
        $persona = Persona::find($id);
        return View::make('persona.edit')
            ->with('persona', $persona);
    }

    public function update($id)
    {
        $rules = array(
            'name'=> 'required',
            'apepat'=> 'required',
            'apemat'=> 'required',
            'address'=> 'required',
            'telephone'=> 'required|numeric',
            'mobile'=> 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('personas/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            $persona = new Persona($id);
            $persona->name = Input::post('name');
            $persona->apepat = Input::post('apepat');
            $persona->apemat = Input::post('apemat');
            $persona->address = Input::post('address');
            $persona->telephone = Input::post('telephone');
            $persona->mobile = Input::post('mobile');
            $persona->save();
            Session::flash('message', 'profesor editado exitosamente!');
            return Redirect::to('personas');
        }
    }
        public function destroy($id) {
        $persona = Persona::find($id);
        //$persona->teacher()->dissociate();
        $persona->delete();
        return Redirect::to('personas')->with('notice', 'El Profesor ha sido eliminado correctamente.');
    }
}

?>