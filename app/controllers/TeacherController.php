<?php 
/**
* 
*/
class TeacherController extends TemplateSchoolController
{
	public function index(){
		
		$jsfiles = array('assets/init/init.mantteacher.view.list.js');
		$teachers =  Persona::join("teacher","person.id_person","=","teacher.id_teacher")->get();
		/*$teachers = DB::table('person')
	    ->join('teacher','teacher.id_teacher','=','person.id_person','left')
	    ->get();*/
	    //$this->layouts = View::make('layouts.default');
	    $title = 'Listado de Profesor';
	    View::share('title', $title);
	    //$data['title'] = $this->layout->title = 'The Home Page';
		return View::make('teacher.index', compact('teachers'))->with('jsfiles', $jsfiles);
	}
	public function create(){
		$title = 'Agregar Profesor';
	    View::share('title', $title);
		return View::make('teacher.create');
	}
	public function store(){
		//validacion
        $rules = array(
            'name'=> 'required',
            'apepat'=> 'required',
            'apemat'=> 'required',
            'address'=> 'required',
            'telephone'=> 'required|numeric',
            'mobile'=> 'required|numeric'
        );
         	        $valid = Validator::make(Input::all(), $rules);
			        if($valid->fails()){
			            return Redirect::to('teacher/create')
			                ->withErrors($valid);
			        }else{
			        $persona = new Persona();
			        $teacher= new Teacher();
			        $persona->name = Input::get('name');
			        $persona->apepat = Input::get('apepat');
			        $persona->apemat = Input::get('apemat');
			        $persona->address = Input::get('address');
			        $persona->telephone = Input::get('telephone');
			        $persona->mobile = Input::get('mobile');
			        $persona->save();
			        $persona->teacher()->save($teacher);
			        $this->logAccesos('Mant. Profesor', 'Creacion', 'Creacion de un Porfesor ');
			        Session::flash('notice', 'Profesor creado exitosamente!');
			        return Redirect::to('teacher');
			         // ->with('notice', 'El Profesor ha sido creado correctamente.');

			        }
        


	}
	public function edit($id){

		$teacher = Persona::find($id);
		return View::make('teacher.edit')
            ->with('teacher', $teacher);

	}
	public function update($id){

		$rules = array(
            'name'=> 'required',
            'apepat'=> 'required',
            'apemat'=> 'required',
            'address'=> 'required',
            'telephone'=> 'required|numeric',
            'mobile'=> 'required|numeric'
        );
		        $validator = Validator::make(Input::all(), $rules);
		        if ($validator->fails()) {
		            return Redirect::to('teacher/' . $id . '/edit')
		                ->withErrors($validator);
		        } else {
		            $persona = Persona::find($id);
		            $persona->name = Input::post('name');
		            $persona->apepat = Input::post('apepat');
		            $persona->apemat = Input::post('apemat');
		            $persona->address = Input::post('address');
		            $persona->telephone = Input::post('telephone');
		            $persona->mobile = Input::post('mobile');
		            $persona->save();
		            Session::flash('message', 'profesor editado exitosamente!');
		            return Redirect::to('teacher');
		        }
        	
        
	}
	public function destroy($id){
		
        try {
        $teacher = Teacher::find($id);
        $teacher->delete();
        $person = Persona::find($id);
        $person->delete();
        } catch (Exception $e) {
        	
        }
        Session::flash('message', 'Successfully deleted the Teacher!');
        return Redirect::to('teacher');

	}

}

?>