<?php

class TemplateSchoolController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected $layout = 'layouts.default';
	public    $title = '';
	
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	    public function __construct()
    {
        // Perform CSRF check on all post/put/patch/delete requests
        $this->beforeFilter('csrf', array('on' => array('post', 'put', 'patch', 'delete')));
    }
	protected function logAccesos($modulo, $motivo, $description){
				Session::keep(array('key', 'email'));
			    $userLog = Session::get('key');
			    $logg = new SysLog();
			    $dt = new DateTime('now');
			    //$start_day = date("Y-m-d", strtotime($date));
			    $logg->fecha = "2050-11-11 16:01:38";
			    $logg->id_user = 1;
			    $logg->ip = $_SERVER['REMOTE_ADDR'];
			    $logg->motivo = $motivo;
			    $logg->module =$modulo;
			   	$logg->description= $description;
			   	$logg->save();
	}


}
