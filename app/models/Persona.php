<?php
class Persona extends Eloquent {
    protected $table = 'person';
    protected $primaryKey = 'id_person';

    public function teacher()
  {
  	return $this->hasOne('Teacher', 'id_teacher');
  }


  public function delete()
    {
        if(count($this->teacher) > 0){
            $this->teacher()->detach();
        }
        return parent::delete();
    } 

  
}
?>