<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/',
	array(
		'as'=>'loginn',
		'uses'=>'LoginController@getlogin'
		)
);
/*
Route::controller('login', 'LoginController');
//Route::resource('personas', 'PersonaController');
Route::resource('teacher', 'TeacherController');
Route::resource('logs', 'LogController');
Route::resource('test','TestController');
Route::resource('notas','NoteController');
//Route::get('personas', array('uses' => 'PersonaController@index'));
/* Home routes */
//Route::controller('/', 'BlogController');



Route::group(array('before'=>'guest'),function(){
	//csrf
	Route::group(array('before'=>'csrf'),function(){
		//postlogin
		Route::post('login',
			array(
				'as'=>'login-in-post',
				'uses'=>'LoginController@postLogin'
				)
		);
		Route::get('login',
			array('as'=>'login',
				'uses'=>'LoginController@getlogin')
			);

	});
	Route::get('/',
			array(
				'as'=>'login',
				'uses'=>'LoginController@getlogin'
				)
		);
				Route::get('/MasterTable',
					array(
						'as' => 'mastertable',
						'uses' => 'MasterTableController@lista'
					)
				);

});

Route::group(array('before'=>'auth'), function(){

	Route::group(array('before'=>'csrf'), function(){

				Route::post('/MasterTable/new',
					array(
						'as' => 'mastertable-save',
						'uses' => 'MasterTableController@savetbl'
						)
				);
					Route::get('/MasterTable',
					array(
						'as' => 'mastertable',
						'uses' => 'MasterTableController@lista'
						)
				);
			Route::post('/MasterTable/update/{param}',
					array(
						'as' => 'mastertable-update',
						'uses' => 'MasterTableController@updatetbl'
						)
				);




		});
	Route::get('/dashboard',
					array(
						'as' => 'dashboard',
						'uses' => 'DashBoardController@Welcome'
					)
				);
	Route::get('/MasterTable',
					array(
						'as' => 'mastertable',
						'uses' => 'MasterTableController@lista'
						)
				);
	Route::get('/MasterTable/new',
					array(
						'as' => 'mastertable-new',
						'uses' => 'MasterTableController@newtbl'
						)
				);
	Route::get('/MasterTable/edit/{param}',
					array(
						'as' => 'mastertable-edit',
						'uses' => 'MasterTableController@edittbl'
						)
				);
				Route::get('/MasterTable/dardealta/{param}',
					array(
						'as' => 'mastertable-dardealta',
						'uses' => 'MasterTableController@dardealta'
						)
				);
			Route::get('/MasterTable/dardebaja/{param}',
					array(
						'as' => 'mastertable-dardebaja',
						'uses' => 'MasterTableController@dardebaja'
						)
				);



});