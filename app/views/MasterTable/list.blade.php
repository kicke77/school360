@extends('layouts.default')
@section('content')





<div class="row">
  <div class="col-md-6 col-md-offset-3">
             <h1> Listado Tablas </h1>
  </div>
</div>
<div class="row">
  <div class="col-md-4">
      
  </div>
  <div class="col-md-4 col-md-offset-4">
            <a href="{{ URL::route('mastertable-new') }}" class="btn btn-success" data-toggle="modal" href="#myModal" >Nueva Tabla</a>
  </div>
</div>
<br>
<div class="row">
        <div class="col-lg-12">
            @if($mastertbl->count())
            <div class="table-responsive">
                <table style="" class="table table-hover table-condensed table-responsive">
                    <thead>
                        <tr>
                            <th > Titulo </th>
                            <th > Descripcion </th>
                            <th > Funcion </th>
                            <th > Todos los Usuarios</th>
                            <th > Estado</th>
                            <th > accion </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($mastertbl as $item)
                        <tr>
                            <td> {{ $item->tx_titulo }} </td>
                            <td> {{ $item->tx_descripcion }} </td>
                            <td> {{ $item->tx_function }} </td>
                            <td> {{ $item->tx_alluser }} </td>
                            <td>
							@if($item->est=='1')
									<span class="label label-success">Activo</span>
							@else
									<span class="label label-danger">Inactivo</span>
							@endif
							
                            </td>
                            <td> 
                            <a class="edittbl" href="{{ URL::route('mastertable-edit',$item->id) }}"><i class="fa fa-edit"></i></a>
                            <a href="{{ URL::route('mastertable-dardealta',$item->id) }}" class="darDeAltaTabla" title="Dar de Alta"><i class="fa fa-check-circle"></i></a>
                            <a href="{{ URL::route('mastertable-dardebaja',$item->id) }}" class="darDeBajaTabla" title="Dar de Baja"><i class="fa fa-trash-o"></i></a>
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <p> No se han encontrado usuarios </p>
            @endif
        </div>

</div>



@stop