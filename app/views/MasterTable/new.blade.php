@extends('layouts.default')
@section('content')

	        <h1> Ingresar Datos de Tabla</h1>
<form method="post" action="{{ URL::route('mastertable-save') }}" role="form"> 

	<div class="form-group{{ ($errors->has('old_password')) ? ' has-error' : '' }}">
		<label for="titulo">Titulo:</label>
		<input type="titulo" class="form-control" id="titulo" name="titulo" />
	</div>
	<div class="form-group{{ ($errors->has('old_password')) ? ' has-error' : '' }}">
		<label for="descripcion">Descripcion:</label>
		<input type="descripcion" class="form-control" id="descripcion" name="descripcion" />
	</div>
	<div class="form-group{{ ($errors->has('old_password')) ? ' has-error' : '' }}">
		<label for="funcion">Funcion:</label>
		<input type="funcion" class="form-control" id="funcion" name="funcion" />
	</div>


	<button type="submit" class="btn btn-success">Guardar</button>
	<a href="{{ URL::route('mastertable') }}" class="btn btn-primary">Volver</a>
	{{ Form::token() }}

</form>
@stop
