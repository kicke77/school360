

@extends('layouts.default')
@section('content')
 {{ HTML::style('css/filename.css') }}


<div class="row">
  <div class="col-md-6 col-md-offset-3">
             <h1> Logs </h1>	
            @if(Session::has('notice'))
            <p> <strong> {{ Session::get('notice') }} </strong> </p>
            @endif
  </div>
</div>
<div class="row">
    <div class="col-lg-12" id="divbuscar">
        <form id="frmFiltros">
            <table class="table" style="margin-bottom:0">
                <tbody>
                    
                    <tr>
                        <td valign="middle">
                            Usuario :
                        </td>
                        <td valign="middle" style="">
                            <input type="text" name="fil_usuario" id="fil_usuario" class="form-control" />
                        </td>
                        <td>
                            Módulo :
                        </td>
                        <td valign="middle" style="width:205px">
                            <input type="text" name="fil_modulo" id="fil_modulo" class="form-control" />
                        </td>
                        <td>
                            Descripción :
                        </td>
                        <td valign="middle" style="width:205px">
                            <input type="text" name="fil_descripcion" id="fil_descripcion" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" style="width:120px">
                            Fecha Inicio:
                        </td>
                        <td valign="middle" style="padding:0 !important">
                            <div style="width:210px; margin-left: 4px" class="input-group input-append date" id="divFechaInicio"  data-date="" data-date-format="dd-mm-yyyy">
                                <input class="form-control" id="fch_ini" name="fch_ini"  type="text" value=""  readonly >
                                <span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span>
                            </div>
                        </td>
                        <td valign="middle">
                            Fecha Final:
                        </td>
                        <td valign="middle" style="padding:0 !important">
                            <div style="margin-left: 4px" class="input-group input-append date" id="divFechaFinal" data-date="" data-date-format="dd-mm-yyyy">
                                <input class="form-control" id="fch_fin" name="fch_fin" type="text" value=""  readonly>
                                <span id="fch2" class="input-group-addon add-on"><i class="fa fa-calendar"></i></span> 
                            </div>
                        </td>
                        <td>
                            IP :
                        </td>
                        <td valign="middle" style="width:205px">
                            <input type="text" name="fil_ip" id="fil_ip" class="form-control" />
                        </td>
                        <td valign="middle"></td>
                        <td valign="middle"></td>
                        <td valign="middle"></td>
                        <td valign="middle" align="right" style="padding:0 !important">
                            <button class="btn btn-primary" style="width:150px" type="submit">Buscar</button>
                        </td>

                    </tr>
                    
            </table>
        </form>
    </div>
</div>
<br>
<div class="row">
        <div class="col-lg-12">
            @if($logs->count())
            <div class="table-responsive">
                <table style="" class="table table-hover table-condensed table-responsive">
                    <thead>
                        <tr>
                            <th > Fecha </th>
                            <th > Nombre Usuario </th>
                            <th > Ip </th>
                            <th > Motivo</th>
                            <th > Modulo </th>
                            <th > Descripcion </th>
                            <th > Acciones </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($logs as $item)
                        <tr>
                            <td> {{ $item->fecha }} </td>
                            <td> {{ $item->id_user }} </td>
                            <td> {{ $item->ip }} </td>
                            <td> {{ $item->motivo }} </td>
                            <td> {{ $item->module }} </td>
                            <td> {{ $item->description }} </td>
                            <td> 
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <p> No se han encontrado usuarios </p>
            @endif
        </div>

</div>


<div class="row">
    <div class="col-lg-12 form-inline">
        <table class="table" border="0">
            <tbody>
                <tr>
                    <td>
                        <label>
                            <select id="quantity-reg" class="form-control" size="1">
                                <option value="10">10</option>
                                <option value="100">100</option>
                                <option value="1000">1000</option>
                            </select>
                            Registros por Página
                        </label>
                    </td>
                    <td style="text-align: center;">
                        <label>
                            <input type="text" value="1" id="currentpage" size="1" class="form-control"/>
                            de 
                            <span id="pages"><?php // echo $pages; ?></span>
                            Página(s)
                        </label>
                    </td>
                    <td style="text-align: right; font-weight: bold;">
                        Mostrando 
                        <span id="reg-start"><?php // echo $startReg; ?></span> 
                        al
                        <span id="reg-end"><?php //echo $endReg; ?></span> 
                        de
                        <span id="total-rows"><?php // echo $quantityReg; ?></span> 
                        registros
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
    @stop
    <div class="modal" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Ingresar Datos</h4>
            </div>
            <div class="modal-body">
              <form role="form">
                    <div class="form-group">
                         <label for="name">Nombres</label>
                         <input class="form-control" id="name" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Apellidos Paterno</label>
                         <input class="form-control" id="aptpat" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Apellido Materno</label>
                         <input class="form-control" id="apemat" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Direccion</label>
                         <input class="form-control" id="address" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Telefono</label>
                         <input class="form-control" id="telephone" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Celular</label>
                         <input class="form-control" id="mobile" type="input" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <a href="#" data-dismiss="modal" class="btn">Volver</a>
              <a href="#" class="btn btn-primary">Guardar</a>
            </div>
          </div>
        </div>
    </div>




    @stop