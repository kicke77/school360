<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
    @section('title')
    <title>{{{$title}}}</title>
    @show
	{{ HTML::style('assets/css/bootstrap.min.css') }}
	{{ HTML::style('assets/components/bootstrap3/css/bootstrap-theme.min.css') }}
	{{ HTML::style('assets/css/calendar.css') }}
	{{ HTML::style('assets/css/sb-admin-2.css') }}
	{{ HTML::style('assets/font-awesome-4.1.0/css/font-awesome.min.css') }}
	
	  <style type="text/css">
    .btn-twitter {
      padding-left: 30px;
      background: rgba(0, 0, 0, 0) url(https://platform.twitter.com/widgets/images/btn.27237bab4db188ca749164efd38861b0.png) -20px 9px no-repeat;
    }
    .btn-twitter:hover {
      background-position:  -21px -16px;
    }
  </style>
</head>
<body>
	
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">School 360</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                   
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>

                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>

                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> Usuario - Perfil</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Opcione</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> COnfiguracion</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesion</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="indexschool.html"><i class="fa fa-dashboard fa-fww"></i> Modulo Asistencia</a>
                        </li>
                        <li>
                            <a href="indexschool2.html"><i class="fa fa-dashboard fa-fww"></i> Modulo Notas</a>
                            
                        </li>
                        <li>
                            <a href="indexschool3.html"><i class="fa fa-dashboard fa-fww"></i> Modulo Profesor</a>
                            
                        </li>


                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            @yield('content')
            <div class="row">
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    {{ HTML::script('./assets/js/jquery.min.js') }}
    <!-- lala -->
	{{ HTML::script('./assets/components/underscore/underscore-min.js') }}
    <!-- end -->
    <!-- Bootstrap Core JavaScript -->
    {{ HTML::script('./assets/js/bootstrap.min.js') }}
    <!-- if--> 
    {{ HTML::script('./assets/components/jstimezonedetect/jstz.min.js') }}
    {{ HTML::script('./assets/js/language/es-ES.js') }}
    {{ HTML::script('./assets/js/calendar.js') }}
    {{ HTML::script('./assets/js/app.js') }}
    <!-- end -->
      <script type="text/javascript">
        var disqus_shortname = 'bootstrapcalendar'; // required: replace example with your forum shortname
        (function() {
          var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
          dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
          (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>

    @section('js')
    @show
</body>
</html>