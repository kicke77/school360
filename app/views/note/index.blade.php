@extends('layouts.default')
@section('content')
		<div id="page-heading">
            <ol class="breadcrumb">
				<li><a href="index.htm">Dashboard</a></li>
				<li>Extras</li>
				<li class="active">Notas</li>
			</ol>

            <h1>Notas</h1>
            <div class="options">
                <div class="btn-toolbar">
					<div class="btn-group hidden-xs">
						<a href='#' class="btn btn-default dropdown-toggle" data-toggle='dropdown'><i class="fa fa-cloud-download"></i><span class="hidden-sm"> Exportar a  </span><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Text File (*.txt)</a></li>
							<li><a href="#">Excel File (*.xlsx)</a></li>
							<li><a href="#">PDF File (*.pdf)</a></li>
						</ul>
					</div>
					<a href="#" class="btn btn-default"><i class="fa fa-cog"></i></a>
				</div>
            </div>
        </div>


        <div class="container">
            <div class="row">
            	<div class="col-md-6">
            		<div class="panel panel-midnightblue">
						<div class="panel-heading">
							<h4>Notas</h4>
						</div>
						<div class="panel-body">
							<p></p>
							<div class="table-responsive">
								<table id="cursos" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th></th>
											<th colspan="4" >
												Notas Promedio
											</th>
										</tr>
										<tr>
											<th>
												 <a href="#" id="curso1">curso </a>
											</th>
											<th>
												B1
											</th>
											<th>
												B2
											</th>
											<th>
												B3
											</th>
											<th>
												B4
											</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<th>Biologia</th>
											<td>13</td>
											<td>15</td>
											<td>--</td>
											<td>--</td>
										</tr>
										<tr>
											<th>Matematica</th>
											<td>10</td>
											<td>12</td>
											<td>--</td>
											<td>--</td>
										</tr>
										<tr>
											<th>Lenguaje</th>
											<td>11</td>
											<td>14</td>
											<td>--</td>
											<td>--</td>
										</tr>
										<tr>
											<th>Historia</th>
											<td>14</td>
											<td>15</td>
											<td>--</td>
											<td>--</td>
										</tr>
										<tr>
											<th>Ciencia</th>
											<td>13</td>
											<td>15</td>
											<td>--</td>
											<td>--</td>
										</tr>
										<tr>
											<th></th>
											<td>12</td>
											<td>14</td>
											<td>--</td>
											<td>--</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
            	</div>
            	<div class="col-md-6">
            		<div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4>Linea Grafica</h4>
                                <div class="options">
                                </div>
                            </div>
                            <div class="panel-body">
                                <div id="line-example" style="position: relative;"></div>
                            </div>
                    </div>
            	</div>
				
            </div>
            <div id="cursosdet" class="row">
            	<div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                            	<h4>Curso Biologia - B1</h4>
                            	<p class="pull-right">11.4</p>
                            </div>
                            <div class="panel-body">
                                <div class="dd" id="nestable_list_1">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="2">
                                            <div class="dd-handle">Notas de Tareas <p class="pull-right">11.4</p></div>
                                            
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <div class="dd-handle">Tarea 1 <p class="pull-right">11.4</p></div>
                                                    
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class="dd-handle">Tarea 2 <p class="pull-right">11.4</p></div>
                                                    
                                                </li>
                                                <li class="dd-item" data-id="5">
                                                    <div class="dd-handle">Tarea 3 <p class="pull-right">11.4</p></div>
                                                    
                                                </li>
                                            </ol>
                                        </li>

                                        <li class="dd-item" data-id="11">
                                            <div class="dd-handle">Notas Orales <p class="pull-right">11.4</p></div>
                                            
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">Oral 1<p class="pull-right">11.4</p></div>
                                                    
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class="dd-handle">Oral 2<p class="pull-right">11.4</p></div>
                                                </li>
                                                <li class="dd-item" data-id="5">
                                                    <div class="dd-handle">Oral 3<p class="pull-right">11.4</p></div>
                                                </li>
                                            </ol>
                                        </li>
                                        <li class="dd-item" data-id="12">
                                            <div class="dd-handle">Participacion<p class="pull-right">11.4</p></div>
                                        </li>
                                        <li class="dd-item" data-id="12">
                                            <div class="dd-handle">Practicas</div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="3">
                                                    <div class="dd-handle">Practica 1<p class="pull-right">11.4</p></div>
                                                </li>
                                                <li class="dd-item" data-id="4">
                                                    <div class="dd-handle">Practica 2<p class="pull-right">11.4</p></div>
                                                </li>
                                                <li class="dd-item" data-id="5">
                                                    <div class="dd-handle">Practica 3<p class="pull-right">11.4</p></div>
                                                </li>
                                                <li class="dd-item" data-id="5">
                                                    <div class="dd-handle">Practica 4<p class="pull-right">11.4</p></div>
                                                </li>
                                                <li class="dd-item" data-id="5">
                                                    <div class="dd-handle">Practica 5<p class="pull-right">11.4</p></div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
            	</div>
            	<div class="col-md-6">
            		    <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4>Linea Grafica</h4>
                                <div class="options">
                                </div>
                            </div>
                            <div class="panel-body">
                                <div id="line-example2" style="position: relative;"></div>
                        </div>
                    </div>
            	</div>
            </div>




  <div class="modal fade" id="modal-container-876216" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h4 class="modal-title" id="myModalLabel">
										Redactar Email
									</h4>
								</div>

								<div class="modal-body">

									 <form role="form" action="" method="post" >
									    <div class="col-lg-12">
									     <div class="form-group">
									        <label for="InputEmail">Para</label>
									        <div class="input-group">
									          <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="aula01@netschool.com.pe" required  >
									          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
									      </div>
									      <div class="form-group">
									        <label for="InputName">Asunto</label>
									        <div class="input-group">
									          <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Enter Asunto Ej. Actividades para el feriado" required>
									          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
									      </div>
									      <div class="form-group">
										      <label for="ejemplo_archivo_1">Adjuntar un archivo</label>
										      <input type="file" id="ejemplo_archivo_1">
										    </div>
									      
									      <div class="form-group">
									        <label for="InputMessage">Mensaje</label>
									        <div class="input-group">
									         <textarea name="ckeditor" id=""  class="ckeditor"></textarea>
									          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
									         </div>
									      </div>
									      <input type="submit" name="submit" id="submit" value="Enviar Ok!" class="btn btn-info pull-right"><br>
									    </div>
									  </form>							
								</div>
								<div class="modal-footer">
									 <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> <button type="button" class="btn btn-primary">Guardar borrador</button>
								</div>
							</div>
							
						</div>
						
					</div>


@stop