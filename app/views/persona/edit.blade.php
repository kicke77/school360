@extends('master')
@section('content')
        <h1> Editar {{ $persona->name }}</h1>
        {{ HTML::ul($errors->all()) }}
        {{ Form::open(array('url' => 'personas/' . $persona->id,  'class' => 'clearfix')) }}
        {{ Form::model($persona, array('route' => array('personas.update', $persona->id_person), 'method' => 'POST')) }}
             <div class="form-group">
            {{ Form::label('name', 'Nombres') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
            </div>
            {{ Form::label ('apepat', 'Apellido Paterno') }}
            <br />
            {{ Form::text ('apepat', Input::old('apepat'), array('class' => 'form-control')) }}            
            <br /> 
             {{ Form::label ('apemat', 'Apellido Materno') }}
            <br />
            {{ Form::text ('apemat', Input::old('apemat'), array('class' => 'form-control')) }}            
            <br /> 

             {{ Form::label ('address', 'Direccion') }}
            <br />
            {{ Form::text ('address', Input::old('address'), array('class' => 'form-control')) }}            
            <br /> 

             {{ Form::label ('telephone', 'Telefono') }}
            <br />
            {{ Form::text ('telephone', Input::old('telephone'), array('class' => 'form-control')) }}            
            <br /> 

             {{ Form::label ('mobile', 'Celular') }}
            <br />
            {{ Form::text ('mobile', Input::old('mobile'), array('class' => 'form-control')) }}            
            <br /> 
            {{ Form::submit('Guardar usuario', array('class' => 'btn btn-primary')) }}
            {{ link_to('personas', 'Cancelar') }}
        {{ Form::close() }}
 

 @stop