

@extends('master')
@section('content')


<div class="row">
  <div class="col-md-6 col-md-offset-3">
             <h1> Listado Profesor </h1>
            @if(Session::has('notice'))
            <p> <strong> {{ Session::get('notice') }} </strong> </p>
            @endif
  </div>
</div>
<div class="row">
  <div class="col-md-4">
      
  </div>
  <div class="col-md-4 col-md-offset-4">
       {{ link_to ('personas/create', 'Nuevo Profesor') }} 
            <a data-toggle="modal" href="#myModal" class="btn btn-success">Nuevo Profesor</a>
  </div>
</div>
<br>
<div class="row">
        <div class="col-lg-12">
            @if($personas->count())
            <div class="table-responsive">
                <table style="" class="table table-hover table-condensed table-responsive">
                    <thead>
                        <tr>
                            <th > Nombres </th>
                            <th > Apellido Paterno </th>
                            <th > Apellido Materno </th>
                            <th > Direccion</th>
                            <th > Telefono </th>
                            <th > Celular </th>
                            <th > Acciones </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($personas as $item)
                        <tr>
                            <td> {{ $item->name }} </td>
                            <td> {{ $item->apepat }} </td>
                            <td> {{ $item->apemat }} </td>
                            <td> {{ $item->address }} </td>
                            <td> {{ $item->telephone }} </td>
                            <td> {{ $item->mobile }} </td>
                            <td> 
                            <a class="editarProfesor" href="{{ URL::to('personas/' . $item->id_person . '/edit') }}"><i class="fa fa-edit"></i></a>
                            <!-- <a href="#" class="eliminarProfesor" title="Eliminar"><i class="fa fa-trash-o"></i></a> -->
                            
                                {{ Form::open(array('url' => 'personas/' . $item->id_person, 'class' => 'fa fa-trash-o')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Eliminar', array('class' => '')) }}
                                {{ Form::close() }}
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <p> No se han encontrado usuarios </p>
            @endif
        </div>

</div>


<div class="row">
    <div class="col-lg-12 form-inline">
        <table class="table" border="0">
            <tbody>
                <tr>
                    <td>
                        <label>
                            <select id="quantity-reg" class="form-control" size="1">
                                <option value="10">10</option>
                                <option value="100">100</option>
                                <option value="1000">1000</option>
                            </select>
                            Registros por Página
                        </label>
                    </td>
                    <td style="text-align: center;">
                        <label>
                            <input type="text" value="1" id="currentpage" size="1" class="form-control"/>
                            de 
                            <span id="pages"><?php // echo $pages; ?></span>
                            Página(s)
                        </label>
                    </td>
                    <td style="text-align: right; font-weight: bold;">
                        Mostrando 
                        <span id="reg-start"><?php // echo $startReg; ?></span> 
                        al
                        <span id="reg-end"><?php //echo $endReg; ?></span> 
                        de
                        <span id="total-rows"><?php // echo $quantityReg; ?></span> 
                        registros
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


    @stop
    <div class="modal" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Ingresar Datos</h4>
            </div>
            <div class="modal-body">
              <form role="form">
                    <div class="form-group">
                         <label for="name">Nombres</label>
                         <input class="form-control" id="name" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Apellidos Paterno</label>
                         <input class="form-control" id="aptpat" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Apellido Materno</label>
                         <input class="form-control" id="apemat" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Direccion</label>
                         <input class="form-control" id="address" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Telefono</label>
                         <input class="form-control" id="telephone" type="input" />
                    </div>

                    <div class="form-group">
                         <label for="name">Celular</label>
                         <input class="form-control" id="mobile" type="input" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <a href="#" data-dismiss="modal" class="btn">Volver</a>
              <a href="#" class="btn btn-primary">Guardar</a>
            </div>
          </div>
        </div>
    </div>