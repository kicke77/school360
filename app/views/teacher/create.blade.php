@extends('layouts.default')
@section('content')
        <h1> Ingresar Datos Profesor</h1>
        {{ HTML::ul($errors->all()) }}
        {{ Form::open(array('url' => 'teacher',  'class' => 'col-md-3 .col-md-offset-3')) }}
            {{ Form::label ('name', 'Nombres') }}
            <br />
            {{ Form::text ('name',  Input::old('name'), array('class' => 'form-control')) }}
            <br />
            {{ Form::label ('apepat', 'Apellido Paterno') }}
            <br />
            {{ Form::text ('apepat', Input::old('apepat'), array('class' => 'form-control')) }}            
            <br /> 
             {{ Form::label ('apemat', 'Apellido Materno') }}
            <br />
            {{ Form::text ('apemat', Input::old('apemat'), array('class' => 'form-control')) }}            
            <br /> 

             {{ Form::label ('address', 'Direccion') }}
            <br />
            {{ Form::text ('address', Input::old('address'), array('class' => 'form-control')) }}            
            <br /> 

             {{ Form::label ('telephone', 'Telefono') }}
            <br />
            {{ Form::text ('telephone', Input::old('telephone'), array('class' => 'form-control')) }}            
            <br /> 

             {{ Form::label ('mobile', 'Celular') }}
            <br />
            {{ Form::text ('mobile', Input::old('mobile'), array('class' => 'form-control')) }}            
            <br /> 
            {{ Form::submit('Guardar usuario', array('class' => 'btn btn-primary')) }}
            {{ link_to('teacher', 'Cancelar') }}
        {{ Form::close() }}
 

 @stop