@extends('layouts.default')
@section('content')
		<div id="page-heading">
            <ol class="breadcrumb">
				<li><a href="index.htm">Dashboard</a></li>
				<li>Extras</li>
				<li class="active">Bandeja de Entrada</li>
			</ol>

            <h1>Bandeja de Entrada</h1>
            <div class="options">
                <div class="btn-toolbar">
					<div class="btn-group hidden-xs">
						<a href='#' class="btn btn-default dropdown-toggle" data-toggle='dropdown'><i class="fa fa-cloud-download"></i><span class="hidden-sm"> Exportar a  </span><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Text File (*.txt)</a></li>
							<li><a href="#">Excel File (*.xlsx)</a></li>
							<li><a href="#">PDF File (*.pdf)</a></li>
						</ul>
					</div>
					<a href="#" class="btn btn-default"><i class="fa fa-cog"></i></a>
				</div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-md-3">
                		<div class="panel">
						<a href="#modal-container-876216" class="btn btn-primary col-xs-12" data-toggle="modal">Redactar Nuevo</a>
					</div>
					<hr>
					<div class="panel">
						<div class="list-group"> 
							<a href="#" class="list-group-item"><span class="badge badge-primary">27</span>Bandeja de Entrada</a> 
							<a href="#" class="list-group-item"><span class="badge badge-danger">9</span>Papepela</a> 
							<a href="#" class="list-group-item">Borrador</a> 
							<a href="#" class="list-group-item">Enviar</a> 
							<a href="#" class="list-group-item"><span class="badge badge-inverse">14</span>Eliminado</a> 
							<a href="#" class="list-group-item"><span class="badge badge-success">5</span>Importante</a> 
						</div>
					</div>
                </div>
                <div class="col-md-9">

					<div class="panel panel-gray">
						<div class="panel-heading">
							<h4>Bandeja de Entrada</h4>
							<div class="options">

							</div>
						</div>
						<div class="panel-body">
							<form class="form-inline" action="#">
			                    <div class="input-group">
				                    <input type="text" placeholder="Buscar mensajes..." class="form-control">
				                    <div class="input-group-btn">
				                    	<button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
				                    </div>
			                    </div>
		                    </form>
		                    <hr>
							<table class="table table-striped table-advance table-hover mailbox">
								<thead>
									<tr>
										<th width="5%"><span><input type="checkbox"></span></th>
										<th colspan="1">
											<div class="btn-group">
												<a class="btn btn-sm btn-default dropdown-toggle" href="#" data-toggle="dropdown"> Accion <i class="fa fa-caret-down"></i>
												</a>
												<ul class="dropdown-menu">
													<li><a href="#">Marcar como Leido</a></li>
													<li><a href="#">Marcar como no Leido</a></li>
													<li><a href="#">Papelera</a></li>
													<li><a href="#">Mover</a></li>
													<li class="divider"></li>
													<li><a href="#">Eliminar</a></li>
												</ul>
											</div>
										</th>
										<th colspan="4">
											<div class="pull-right">
												<span class="pagination-info" style="margin-right:5px">1-30 of 789</span>
												<a class="btn btn-sm btn-default"><i class="fa fa-angle-left"></i></a>
												<a class="btn btn-sm btn-default"><i class="fa fa-angle-right"></i></a>
											</div>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr class="unread">
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Posiedon Web</td>
										<td>Server undergoing maintainence</td>
										<td><i class="fa fa-paperclip"></i></td>
										<td class="text-right">16:30 PM</td>
									</tr>
									<tr class="unread">
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Douglas Adams</td>
										<td>Waiting for documentation</td>
										<td></td>
										<td class="text-right">May 5</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">The Doctor</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td></td>
										<td class="text-right">May 5</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Nero</td>
										<td>Rilate niro lakem, consectetuer adipiscing</td>
										<td></td>
										<td class="text-right">May 4</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Envato</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td></td>
										<td class="text-right">May 5</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">mPower</td>
										<td>Rilate niro lakem, consectetuer adipiscing</td>
										<td><i class="fa fa-paperclip"></i></td>
										<td class="text-right">May 4</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Facebook</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td><i class="fa fa-paperclip"></i></td>
										<td class="text-right">May 5</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Twitter</td>
										<td>Rilate niro lakem, consectetuer adipiscing</td>
										<td></td>
										<td class="text-right">May 4</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Emma Watson</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td></td>
										<td class="text-right">May 5</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Life</td>
										<td>Lemons for sale</td>
										<td></td>
										<td class="text-right">May 4</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">John Doe</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td></td>
										<td class="text-right">May 5</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Jack Smith</td>
										<td>Rilate niro lakem, consectetuer adipiscing</td>
										<td><i class="fa fa-paperclip"></i></td>
										<td class="text-right">May 4</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Indiana Jones</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td><i class="fa fa-paperclip"></i></td>
										<td class="text-right">May 5</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Sherlock</td>
										<td>Rilate niro lakem, consectetuer adipiscing</td>
										<td></td>
										<td class="text-right">May 4</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Facebook</td>
										<td>Rilate niro lakem, consectetuer adipiscing</td>
										<td><i class="fa fa-paperclip"></i></td>
										<td class="text-right">May 4</td>
									</tr>
									<tr>
										<td><span><input type="checkbox"></span></td>
										<td class="hidden-xs">Jane Smith</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td><i class="fa fa-paperclip"></i></td>
										<td class="text-right">May 5</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="5">
											<div class="pull-right">
												<span class="pagination-info" style="margin-right:5px">1-30 of 789</span>
												<a class="btn btn-sm btn-default"><i class="fa fa-angle-left"></i></a>
												<a class="btn btn-sm btn-default"><i class="fa fa-angle-right"></i></a>
											</div>
										</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
				
            </div>




  <div class="modal fade" id="modal-container-876216" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h4 class="modal-title" id="myModalLabel">
										Redactar Email
									</h4>
								</div>

								<div class="modal-body">

									 <form role="form" action="" method="post" >
									    <div class="col-lg-12">
									     <div class="form-group">
									        <label for="InputEmail">Para</label>
									        <div class="input-group">
									          <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="aula01@netschool.com.pe" required  >
									          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
									      </div>
									      <div class="form-group">
									        <label for="InputName">Asunto</label>
									        <div class="input-group">
									          <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Enter Asunto Ej. Actividades para el feriado" required>
									          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
									      </div>
									      <div class="form-group">
										      <label for="ejemplo_archivo_1">Adjuntar un archivo</label>
										      <input type="file" id="ejemplo_archivo_1">
										    </div>
									      
									      <div class="form-group">
									        <label for="InputMessage">Mensaje</label>
									        <div class="input-group">
									         <textarea name="ckeditor" id=""  class="ckeditor"></textarea>
									          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
									         </div>
									      </div>
									      <input type="submit" name="submit" id="submit" value="Enviar Ok!" class="btn btn-info pull-right"><br>
									    </div>
									  </form>							
								</div>
								<div class="modal-footer">
									 <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> <button type="button" class="btn btn-primary">Guardar borrador</button>
								</div>
							</div>
							
						</div>
						
					</div>



<script type="text/javascript">
	CKEDITOR.replace('editor1');
</script>
@stop