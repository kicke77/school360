-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-11-2014 a las 22:26:29
-- Versión del servidor: 5.6.12-log
-- Versión de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `netschool_1`
--
CREATE DATABASE IF NOT EXISTS `netschool_1` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `netschool_1`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_students_by_parent`(IN `_id_parent` INT)
    NO SQL
select 	name,
		apepat,
		apemat 
from 	person
inner join student s on id_person=id_student
inner join parent p on p.id_parent=s.id_parent
where 	s.id_parent=_id_parent$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
  `id_attendance` int(11) NOT NULL,
  `id_std_rom_crs` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id_attendance`),
  KEY `fk_attendance_std_rom_crs1_idx` (`id_std_rom_crs`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id_branch` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `school_id_school` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_branch`),
  KEY `fk_branch_school1_idx` (`school_id_school`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id_course` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_course`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `course`
--

INSERT INTO `course` (`id_course`, `name`, `state`) VALUES
(1, 'matemática', 1),
(2, 'lenguaje', 1),
(3, 'ciencias naturales', 1),
(4, 'química', 1),
(5, 'física', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluation`
--

CREATE TABLE IF NOT EXISTS `evaluation` (
  `id_evaluation` int(11) NOT NULL AUTO_INCREMENT,
  `num` tinyint(4) NOT NULL,
  `id_type_evaluation` int(11) NOT NULL,
  `id_room_course` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_evaluation`),
  KEY `fk_evaluation_type_evaluation1_idx` (`id_type_evaluation`),
  KEY `fk_evaluation_room_course1_idx` (`id_room_course`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eva_std_rom_crs`
--

CREATE TABLE IF NOT EXISTS `eva_std_rom_crs` (
  `id_eva_std_rom_crs` int(11) NOT NULL,
  `note` tinyint(4) NOT NULL,
  `state` varchar(45) NOT NULL,
  `id_std_rom_crs` int(11) NOT NULL,
  `id_evaluation` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id_eva_std_rom_crs`),
  KEY `fk_eva_std_rom_crs_std_rom_crs1_idx` (`id_std_rom_crs`),
  KEY `fk_eva_std_rom_crs_evaluation1_idx` (`id_evaluation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grade`
--

CREATE TABLE IF NOT EXISTS `grade` (
  `id_grade` int(11) NOT NULL AUTO_INCREMENT,
  `num` tinyint(4) NOT NULL,
  `id_level` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_grade`),
  KEY `fk_grade_level1_idx` (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `grade`
--

INSERT INTO `grade` (`id_grade`, `num`, `id_level`, `state`) VALUES
(1, 1, 1, 0),
(2, 2, 1, 0),
(3, 3, 1, 0),
(4, 4, 1, 0),
(5, 5, 1, 0),
(6, 6, 1, 0),
(7, 1, 2, 0),
(8, 2, 2, 0),
(9, 3, 2, 0),
(10, 4, 2, 0),
(11, 5, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `level`
--

INSERT INTO `level` (`id_level`, `name`, `state`) VALUES
(1, 'primaria', 1),
(2, 'secundaria', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `option`
--

CREATE TABLE IF NOT EXISTS `option` (
  `id_option` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_option`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `option`
--

INSERT INTO `option` (`id_option`, `name`, `state`) VALUES
(1, 'dashboard', 1),
(2, 'menustudent', 1),
(3, 'menustudent', 1),
(4, 'menustudent', 1),
(5, 'menustudent', 1),
(6, 'menustudent', 1),
(7, 'menustudent', 1),
(8, 'menustudent', 1),
(9, 'menuteacher', 1),
(10, 'menuteacher', 1),
(11, 'menuteacher', 1),
(12, 'menuteacher', 1),
(13, 'menuteacher', 1),
(14, 'menuteacher', 1),
(15, 'menuprincipal', 1),
(16, 'menuprincipal', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `option_profile`
--

CREATE TABLE IF NOT EXISTS `option_profile` (
  `id_option_profile` int(11) NOT NULL AUTO_INCREMENT,
  `id_profile` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `id_option` int(11) NOT NULL,
  PRIMARY KEY (`id_option_profile`),
  KEY `fk_option_profile_profile1_idx` (`id_profile`),
  KEY `fk_option_profile_option1_idx` (`id_option`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `option_profile`
--

INSERT INTO `option_profile` (`id_option_profile`, `id_profile`, `state`, `id_option`) VALUES
(1, 3, 1, 1),
(2, 3, 1, 2),
(3, 3, 1, 3),
(4, 3, 1, 4),
(5, 3, 1, 5),
(6, 3, 1, 6),
(7, 3, 1, 7),
(8, 3, 1, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parent`
--

CREATE TABLE IF NOT EXISTS `parent` (
  `id_parent` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `parent`
--

INSERT INTO `parent` (`id_parent`, `state`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id_person` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `apepat` varchar(45) NOT NULL,
  `apemat` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_person`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=301 ;

--
-- Volcado de datos para la tabla `person`
--

INSERT INTO `person` (`id_person`, `name`, `apepat`, `apemat`, `address`, `telephone`, `mobile`) VALUES
(1, 'Kerry', 'Cruz', 'C.', 'Suite 876', '235-787-46', '345-138-6799'),
(2, 'Bobby', 'Riggs', 'B.', 'Apt No. 797', '906-204-76', '501-281-4599'),
(3, 'Phoebe', 'Riggs', 'G.', 'P.O. Box 7753', '121-499-67', '955-503-5627'),
(4, 'Lynnette', 'Fuentes', 'D.', 'Apt 352', '837-673-39', '703-731-6265'),
(5, 'Jacqueline', 'Britt', 'D.', 'Suite 8165', '187-829-70', '457-565-9483'),
(6, 'Ophelia', 'Worley', 'P.', 'P.O. Box 9189', '212-672-47', '583-925-6315'),
(7, 'Melody', 'Mata', 'L.', 'Suite 9178', '365-941-15', '304-366-1849'),
(8, 'Jeremy', 'Hines', 'H.', 'Apt 569', '135-594-81', '195-672-0594'),
(9, 'Edythe', 'Browning', 'B.', 'Apt 537', '571-783-14', '672-420-2388'),
(10, 'Shanda', 'Camp', 'D.', 'Apt No. 7927', '419-198-72', '796-957-4059'),
(11, 'Octavia', 'Turner', 'J.', 'Apt No. 66', '442-578-77', '260-603-4894'),
(12, 'Grover', 'Sinclair', 'H.', 'Apt 985', '826-035-64', '648-246-1144'),
(13, 'Lewis', 'Morales', 'N.', 'Apt 10', '472-536-01', '519-280-2545'),
(14, 'Alfreda', 'Rucker', 'W.', 'P.O. Box 563', '608-356-57', '246-239-8014'),
(15, 'Monika', 'Rosa', 'E.', 'P.O. Box 977', '948-068-67', '350-991-8396'),
(16, 'Emanuel', 'Riddle', 'P.', 'Apt No. 621', '122-943-78', '281-771-3929'),
(17, 'Pauline', 'Velasquez', 'S.', 'P.O. Box 22', '451-255-89', '804-518-8845'),
(18, 'Jolene', 'Fuller', 'L.', 'Apt No. 221', '962-545-68', '405-115-2475'),
(19, 'Barbara', 'Reid', 'F.', 'Apt No. 589', '947-824-09', '777-828-8797'),
(20, 'Jerri', 'Godfrey', 'B.', 'Apt No. 9655', '444-302-32', '124-094-1234'),
(21, 'Athena', 'Young', 'M.', 'P.O. Box 8138', '392-793-30', '819-769-7645'),
(22, 'Tammi', 'Mcbride', 'E.', 'Suite 4329', '882-010-82', '119-797-2164'),
(23, 'Chauncey', 'Vega', 'B.', 'Suite 13', '506-626-14', '323-247-1147'),
(24, 'Vernice', 'Love', 'H.', 'P.O. Box 6530', '718-189-03', '203-431-3222'),
(25, 'Micah', 'Chan', 'T.', 'Apt 20', '658-112-99', '500-090-8204'),
(26, 'Duncan', 'Huber', 'M.', 'Apt No. 18', '357-002-05', '765-044-8591'),
(27, 'Julian', 'Chan', 'E.', 'Suite 7869', '299-573-06', '491-932-8796'),
(28, 'Shelly', 'Watkins', 'G.', 'P.O. Box 4502', '660-969-25', '841-283-8623'),
(29, 'Augusta', 'Ortiz', 'A.', 'Apt No. 645', '218-708-53', '217-008-1552'),
(30, 'Mitchel', 'Robles', 'U.', 'Suite 81', '449-191-96', '537-525-5157'),
(31, 'Sydney', 'Lynch', 'H.', 'Apt 510', '694-617-31', '943-837-8823'),
(32, 'Emilie', 'Steele', 'J.', 'Apt No. 15', '111-907-64', '258-728-1255'),
(33, 'Emilio', 'Sampson', 'W.', 'Apt No. 94', '562-330-07', '735-261-9547'),
(34, 'Anthony', 'Collier', 'L.', 'Suite 4377', '208-417-06', '935-934-4700'),
(35, 'Carmen', 'Vazquez', 'T.', 'Suite 834', '869-395-04', '403-129-6064'),
(36, 'Guadalupe', 'Barlow', 'M.', 'Apt No. 114', '574-358-04', '141-679-5442'),
(37, 'Leola', 'Miller', 'F.', 'Apt No. 3931', '435-332-32', '439-906-5329'),
(38, 'Lacey', 'Mcfarland', 'D.', 'Apt No. 766', '218-247-35', '595-193-7695'),
(39, 'Ebony', 'Blackwell', 'S.', 'Apt No. 17', '201-672-31', '909-084-2618'),
(40, 'Juana', 'Erickson', 'E.', 'Suite 776', '293-357-71', '444-322-5477'),
(41, 'Letha', 'Carter', 'C.', 'Apt 9390', '338-480-86', '239-372-6232'),
(42, 'Rayford', 'Juarez', 'T.', 'Suite 4541', '434-395-00', '432-996-1723'),
(43, 'Isabel', 'England', 'L.', 'Suite 3259', '315-094-82', '237-366-1959'),
(44, 'Rhea', 'Clayton', 'V.', 'Suite 836', '958-286-90', '686-237-6736'),
(45, 'Mavis', 'Hayes', 'D.', 'P.O. Box 42', '558-398-60', '200-863-5831'),
(46, 'Rosalind', 'Browning', 'E.', 'P.O. Box 9803', '861-303-45', '940-508-4919'),
(47, 'Aileen', 'Gay', 'O.', 'Suite 316', '949-807-24', '777-865-7948'),
(48, 'Maxwell', 'Hernandez', 'D.', 'Suite 882', '853-454-27', '718-867-0219'),
(49, 'Herschel', 'Beck', 'G.', 'Suite 782', '159-308-56', '410-513-9258'),
(50, 'Teresita', 'Howe', 'W.', 'Suite 7406', '510-748-42', '237-669-2260'),
(51, 'Christine', 'Little', 'G.', 'P.O. Box 5356', '574-597-19', '963-797-7351'),
(52, 'Alonzo', 'Murphy', 'U.', 'Suite 190', '713-313-73', '756-039-2160'),
(53, 'Vicky', 'Valdez', 'W.', 'Suite 78', '628-432-85', '834-747-8893'),
(54, 'Kris', 'Lambert', 'J.', 'Suite 54', '472-373-24', '363-062-5243'),
(55, 'Nolan', 'Cooke', 'D.', 'P.O. Box 3624', '155-982-54', '282-188-5416'),
(56, 'Milo', 'White', 'A.', 'P.O. Box 884', '762-443-61', '585-838-7178'),
(57, 'Bonnie', 'Woodward', 'R.', 'Suite 6572', '406-819-77', '359-355-5921'),
(58, 'Dalia', 'Lowery', 'K.', 'P.O. Box 166', '676-294-46', '669-429-3855'),
(59, 'Ramona', 'Humphrey', 'A.', 'P.O. Box 716', '786-551-91', '174-958-7291'),
(60, 'Lazaro', 'Garrett', 'A.', 'P.O. Box 8821', '232-901-37', '846-145-3222'),
(61, 'Mitchell', 'Dickerson', 'W.', 'Apt 4100', '795-191-40', '107-341-1131'),
(62, 'Heriberto', 'Harmon', 'F.', 'Suite 7272', '942-867-11', '929-454-7528'),
(63, 'Earnestine', 'Frazier', 'C.', 'Suite 154', '485-218-12', '471-287-9474'),
(64, 'Lee', 'Bernard', 'W.', 'P.O. Box 59', '725-523-18', '393-680-4682'),
(65, 'Stacy', 'William', 'R.', 'Suite 9267', '147-970-20', '401-965-0896'),
(66, 'Fay', 'Lancaster', 'E.', 'P.O. Box 32', '342-127-05', '297-582-8202'),
(67, 'Malissa', 'Bishop', 'N.', 'Apt No. 576', '429-757-75', '846-841-7873'),
(68, 'Woodrow', 'Lowery', 'T.', 'Apt No. 79', '149-904-78', '763-188-8284'),
(69, 'Cheryl', 'Harden', 'N.', 'Apt No. 379', '287-527-73', '645-330-0422'),
(70, 'Jacques', 'Baird', 'R.', 'Suite 598', '739-249-55', '368-063-9975'),
(71, 'Emilie', 'Sweet', 'A.', 'P.O. Box 5823', '320-080-15', '417-875-2560'),
(72, 'Frankie', 'Warner', 'V.', 'Suite 14', '127-875-37', '555-439-1976'),
(73, 'Mervin', 'Kemp', 'N.', 'Apt 5104', '424-073-81', '480-373-2424'),
(74, 'Breanna', 'Carney', 'O.', 'Apt No. 77', '211-984-78', '685-109-2139'),
(75, 'Ezra', 'Ellison', 'V.', 'Apt No. 646', '146-105-03', '156-781-5194'),
(76, 'Lacy', 'Mayer', 'W.', 'Suite 753', '912-850-60', '457-458-6512'),
(77, 'Kristin', 'Rivers', 'M.', 'Apt 8674', '732-921-79', '865-455-0631'),
(78, 'Micheal', 'Simpson', 'W.', 'P.O. Box 195', '984-809-29', '170-994-5281'),
(79, 'Bernadine', 'Knox', 'C.', 'Suite 6031', '322-082-14', '600-449-8978'),
(80, 'Myrtle', 'Stafford', 'A.', 'Apt 851', '624-023-24', '514-323-6840'),
(81, 'Sylvia', 'Haley', 'S.', 'P.O. Box 357', '748-765-83', '569-157-4628'),
(82, 'Olga', 'Dean', 'A.', 'Suite 3600', '546-772-00', '630-585-7120'),
(83, 'Joanne', 'Womack', 'F.', 'Suite 5524', '397-589-94', '292-219-5728'),
(84, 'Marta', 'Patton', 'S.', 'P.O. Box 76', '832-494-19', '592-130-4392'),
(85, 'Leona', 'Escobar', 'M.', 'Apt No. 68', '703-273-51', '933-904-3889'),
(86, 'Gerald', 'Short', 'H.', 'P.O. Box 6857', '722-128-55', '423-358-3915'),
(87, 'Moshe', 'Helms', 'B.', 'Apt No. 9541', '818-592-14', '709-632-9965'),
(88, 'Sheldon', 'Rhodes', 'J.', 'Apt No. 5132', '397-517-20', '266-051-2697'),
(89, 'Judith', 'Albert', 'J.', 'Apt 133', '308-625-47', '754-167-2236'),
(90, 'Andreas', 'Clay', 'R.', 'Suite 530', '585-574-26', '531-991-4821'),
(91, 'Orville', 'Drake', 'N.', 'P.O. Box 629', '109-617-69', '851-333-4400'),
(92, 'Lenora', 'Norton', 'C.', 'Apt 24', '855-730-04', '385-682-0030'),
(93, 'Abe', 'Bullock', 'P.', 'Apt 404', '168-514-00', '861-812-6025'),
(94, 'Jennifer', 'Kendrick', 'D.', 'Apt No. 34', '884-975-43', '456-890-7082'),
(95, 'Lina', 'Rivers', 'S.', 'Apt No. 4130', '522-402-86', '224-623-5338'),
(96, 'Daniela', 'Brandon', 'A.', 'P.O. Box 22', '742-223-92', '193-124-8970'),
(97, 'Teodoro', 'Barrett', 'B.', 'Apt 2105', '298-157-74', '607-497-6762'),
(98, 'Hung', 'Baker', 'E.', 'Apt 9414', '169-068-21', '517-786-6724'),
(99, 'Winona', 'Gill', 'J.', 'Apt No. 358', '532-953-16', '400-570-3360'),
(100, 'Tammi', 'Dixon', 'H.', 'P.O. Box 92', '595-635-45', '973-976-8398'),
(101, 'Florence', 'Mejia', 'E.', 'Suite 9452', '527-015-84', '210-067-9745'),
(102, 'Wilma', 'Mckinney', 'D.', 'Apt 590', '188-417-20', '643-563-9810'),
(103, 'Cordell', 'Huff', 'M.', 'P.O. Box 59', '206-599-95', '172-351-3810'),
(104, 'Ruby', 'Melendez', 'E.', 'Suite 30', '284-262-71', '318-769-8207'),
(105, 'Harriett', 'Vaughan', 'T.', 'Apt No. 6385', '185-070-26', '167-155-4011'),
(106, 'Mayra', 'Carroll', 'U.', 'Suite 2724', '491-164-00', '486-085-0357'),
(107, 'Burl', 'Ramos', 'B.', 'Apt No. 41', '955-031-44', '409-067-9913'),
(108, 'Miguel', 'Flores', 'B.', 'Apt No. 69', '713-305-45', '399-505-3396'),
(109, 'Sheree', 'Stout', 'G.', 'Apt 31', '465-364-49', '413-704-8833'),
(110, 'Helene', 'Ochoa', 'V.', 'Apt No. 7959', '207-929-90', '796-686-7973'),
(111, 'Geraldine', 'Cooper', 'S.', 'Suite 1737', '329-952-53', '256-647-5336'),
(112, 'Benito', 'Craft', 'N.', 'Suite 323', '406-790-85', '119-953-6529'),
(113, 'Haywood', 'Owens', 'G.', 'P.O. Box 7407', '312-037-24', '888-207-3749'),
(114, 'Christopher', 'Cain', 'M.', 'P.O. Box 974', '550-204-62', '300-606-1710'),
(115, 'Osvaldo', 'Keller', 'T.', 'Apt No. 6382', '802-369-97', '634-588-8077'),
(116, 'Rebecca', 'Benjamin', 'E.', 'Apt No. 88', '320-086-40', '921-744-0816'),
(117, 'Danna', 'Townsend', 'U.', 'P.O. Box 866', '293-228-15', '925-767-9091'),
(118, 'Corey', 'Shaffer', 'J.', 'P.O. Box 100', '286-888-68', '210-727-2861'),
(119, 'Mikel', 'Bunch', 'P.', 'P.O. Box 744', '827-545-90', '805-193-1017'),
(120, 'Catrina', 'Nash', 'M.', 'Apt 6009', '884-424-15', '251-752-3776'),
(121, 'Clarissa', 'Stein', 'P.', 'Apt No. 233', '265-295-44', '569-163-5567'),
(122, 'Sylvia', 'Fuller', 'A.', 'Apt No. 4859', '908-036-29', '116-400-9221'),
(123, 'Dona', 'Galloway', 'H.', 'P.O. Box 7283', '780-009-10', '925-648-3317'),
(124, 'Guillermo', 'Wade', 'S.', 'Apt 548', '801-508-17', '625-558-1825'),
(125, 'Justina', 'Horne', 'O.', 'Apt 844', '542-827-55', '553-080-5877'),
(126, 'Candice', 'Holden', 'V.', 'Apt 880', '697-334-66', '192-789-8097'),
(127, 'Arleen', 'Grace', 'C.', 'P.O. Box 4262', '397-830-41', '370-374-7985'),
(128, 'Juliet', 'Gibson', 'U.', 'Apt 7148', '270-811-32', '911-731-8112'),
(129, 'Daisy', 'Witt', 'F.', 'Apt No. 915', '533-152-59', '439-578-3706'),
(130, 'Brittany', 'Minor', 'E.', 'Apt No. 10', '822-857-12', '367-360-0053'),
(131, 'Emerson', 'Stanley', 'F.', 'P.O. Box 9350', '249-166-22', '528-395-2380'),
(132, 'Aurelio', 'Maddox', 'G.', 'P.O. Box 4580', '341-842-26', '594-265-9546'),
(133, 'Shawna', 'Gomez', 'J.', 'Apt 700', '491-090-44', '190-111-7608'),
(134, 'Oren', 'Pitts', 'K.', 'Suite 803', '137-965-87', '276-365-5236'),
(135, 'Felecia', 'Bond', 'W.', 'Suite 45', '187-938-60', '419-617-1872'),
(136, 'Erna', 'Kramer', 'M.', 'P.O. Box 1538', '445-845-99', '888-600-3925'),
(137, 'Bennie', 'Montgomery', 'W.', 'Suite 607', '765-201-01', '215-168-6042'),
(138, 'Kitty', 'Acevedo', 'L.', 'Suite 8953', '779-852-40', '405-889-1728'),
(139, 'Janet', 'Coffman', 'N.', 'P.O. Box 285', '530-499-91', '464-165-1114'),
(140, 'Lou', 'Randall', 'W.', 'Apt No. 5887', '255-058-29', '317-673-5511'),
(141, 'Vince', 'Schultz', 'J.', 'Apt No. 90', '110-426-06', '215-559-1329'),
(142, 'Raleigh', 'Serrano', 'M.', 'Apt No. 46', '329-011-95', '251-719-1092'),
(143, 'Addie', 'Everett', 'E.', 'Suite 6407', '842-520-34', '117-181-0363'),
(144, 'Robbie', 'Mathis', 'N.', 'Suite 613', '552-614-94', '416-511-0799'),
(145, 'Luella', 'Rivas', 'E.', 'Apt No. 114', '226-103-49', '496-750-5038'),
(146, 'Keith', 'Tyson', 'K.', 'P.O. Box 8404', '915-768-04', '128-506-8347'),
(147, 'Christa', 'Charles', 'P.', 'Suite 57', '792-590-93', '964-808-5758'),
(148, 'Sheldon', 'Bolden', 'T.', 'Apt 7931', '525-155-16', '425-757-3846'),
(149, 'Ismael', 'Stokes', 'A.', 'Suite 994', '107-257-00', '952-730-3339'),
(150, 'Ray', 'Rosales', 'V.', 'Suite 452', '879-106-63', '937-777-4894');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id_profile` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_profile`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `profile`
--

INSERT INTO `profile` (`id_profile`, `name`, `state`) VALUES
(1, 'student', 1),
(2, 'teacher', 1),
(3, 'parent', 1),
(4, 'principal', 1),
(5, 'admin', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `school`
--

CREATE TABLE IF NOT EXISTS `school` (
  `id_school` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  PRIMARY KEY (`id_school`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `id_section` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_section`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `section`
--

INSERT INTO `section` (`id_section`, `name`, `state`) VALUES
(1, 'A', 1),
(2, 'B', 1),
(3, 'C', 1),
(4, 'D', 0),
(5, 'E', 0),
(6, 'F', 0),
(7, 'G', 0),
(8, 'H', 0),
(9, 'I', 0),
(10, 'J', 0),
(11, 'K', 0),
(12, 'L', 0),
(13, 'M', 0),
(14, 'N', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id_student` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL,
  `id_room` int(11) DEFAULT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_student`),
  KEY `fk_student_parent1_idx` (`id_parent`),
  KEY `fk_student_grade_section1_idx` (`id_room`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `student`
--

INSERT INTO `student` (`id_student`, `id_parent`, `id_room`, `state`) VALUES
(31, 1, NULL, 1),
(32, 2, NULL, 1),
(33, 3, NULL, 1),
(34, 4, NULL, 1),
(35, 5, NULL, 1),
(36, 6, NULL, 1),
(37, 7, NULL, 1),
(38, 8, NULL, 1),
(39, 9, NULL, 0),
(40, 10, NULL, 1),
(41, 11, NULL, 1),
(42, 12, NULL, 1),
(43, 13, NULL, 1),
(44, 14, NULL, 1),
(45, 15, NULL, 1),
(46, 16, NULL, 1),
(47, 17, NULL, 1),
(48, 18, NULL, 1),
(49, 19, NULL, 1),
(50, 20, NULL, 1),
(51, 21, NULL, 1),
(52, 22, NULL, 0),
(53, 23, NULL, 1),
(54, 24, NULL, 1),
(55, 25, NULL, 1),
(56, 26, NULL, 1),
(57, 27, NULL, 1),
(58, 28, NULL, 1),
(59, 29, NULL, 1),
(60, 30, NULL, 1),
(61, 1, NULL, 1),
(62, 2, NULL, 1),
(63, 3, NULL, 1),
(64, 4, NULL, 1),
(65, 5, NULL, 1),
(66, 6, NULL, 1),
(67, 7, NULL, 1),
(68, 8, NULL, 0),
(69, 9, NULL, 1),
(70, 10, NULL, 1),
(71, 11, NULL, 1),
(72, 12, NULL, 1),
(73, 13, NULL, 1),
(74, 14, NULL, 1),
(75, 15, NULL, 1),
(76, 16, NULL, 1),
(77, 17, NULL, 1),
(78, 18, NULL, 1),
(79, 19, NULL, 1),
(80, 20, NULL, 1),
(81, 21, NULL, 0),
(82, 22, NULL, 1),
(83, 23, NULL, 1),
(84, 24, NULL, 1),
(85, 25, NULL, 1),
(86, 26, NULL, 1),
(87, 27, NULL, 1),
(88, 28, NULL, 1),
(89, 29, NULL, 1),
(90, 30, NULL, 1),
(91, 1, NULL, 1),
(92, 2, NULL, 1),
(93, 3, NULL, 1),
(94, 4, NULL, 0),
(95, 5, NULL, 1),
(96, 6, NULL, 1),
(97, 7, NULL, 1),
(98, 8, NULL, 1),
(99, 9, NULL, 1),
(100, 10, NULL, 1),
(101, 11, NULL, 1),
(102, 12, NULL, 1),
(103, 13, NULL, 1),
(104, 14, NULL, 1),
(105, 15, NULL, 1),
(106, 16, NULL, 1),
(107, 17, NULL, 1),
(108, 18, NULL, 1),
(109, 19, NULL, 1),
(110, 20, NULL, 1),
(111, 21, NULL, 1),
(112, 22, NULL, 1),
(113, 23, NULL, 1),
(114, 24, NULL, 1),
(115, 25, NULL, 1),
(116, 26, NULL, 1),
(117, 27, NULL, 0),
(118, 28, NULL, 1),
(119, 29, NULL, 1),
(120, 30, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `id_teacher` int(11) NOT NULL,
  `principal` tinyint(1) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_teacher`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `teacher`
--

INSERT INTO `teacher` (`id_teacher`, `principal`, `state`) VALUES
(121, 1, 1),
(122, 0, 1),
(123, 0, 1),
(124, 0, 1),
(125, 0, 1),
(126, 0, 1),
(127, 0, 1),
(128, 0, 1),
(129, 0, 1),
(130, 0, 1),
(131, 0, 1),
(132, 0, 1),
(134, 0, 1),
(135, 0, 1),
(136, 0, 1),
(137, 0, 1),
(138, 0, 1),
(139, 0, 1),
(140, 0, 1),
(141, 0, 1),
(142, 0, 1),
(143, 0, 1),
(144, 0, 1),
(145, 0, 1),
(146, 0, 1),
(147, 0, 1),
(148, 0, 1),
(149, 0, 1),
(150, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_evaluation`
--

CREATE TABLE IF NOT EXISTS `type_evaluation` (
  `id_type_evaluation` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_type_evaluation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `type_evaluation`
--

INSERT INTO `type_evaluation` (`id_type_evaluation`, `name`, `state`) VALUES
(1, 'tarea', 1),
(2, 'práctica', 1),
(3, 'exámen', 1),
(4, 'trabajo', 1),
(5, 'exposición', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_profile` int(11) NOT NULL,
  `id_person` int(11) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(60) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `nsesions` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `fk_user_profile1_idx` (`id_profile`),
  KEY `fk_user_person1_idx` (`id_person`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_user`, `id_profile`, `id_person`, `login`, `password`, `state`, `nsesions`) VALUES
(1, 3, 1, 'kcruzc', '$2a$10$qjhCLLMA87Vmjy/AmF6U9e6NcvkVoQkkkvFNYC/xScGKZnElYmp.W', 1, 0),
(2, 3, 2, 'briggsb', '$2a$10$qjhCLLMA87Vmjy/AmF6U9e6NcvkVoQkkkvFNYC/xScGKZnElYmp.W', 1, 0),
(3, 3, 3, 'priggsg', '$2a$10$qjhCLLMA87Vmjy/AmF6U9e6NcvkVoQkkkvFNYC/xScGKZnElYmp.W', 1, 0),
(4, 3, 4, 'lfuentesd', '$2a$10$qjhCLLMA87Vmjy/AmF6U9e6NcvkVoQkkkvFNYC/xScGKZnElYmp.W', 1, 0),
(5, 3, 5, 'jbrittd', '$2a$10$qjhCLLMA87Vmjy/AmF6U9e6NcvkVoQkkkvFNYC/xScGKZnElYmp.W', 1, 0);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `fk_attendance_std_rom_crs1` FOREIGN KEY (`id_std_rom_crs`) REFERENCES `std_rom_crs` (`id_std_rom_crs`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `branch`
--
ALTER TABLE `branch`
  ADD CONSTRAINT `fk_branch_school1` FOREIGN KEY (`school_id_school`) REFERENCES `school` (`id_school`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `evaluation`
--
ALTER TABLE `evaluation`
  ADD CONSTRAINT `fk_evaluation_room_course1` FOREIGN KEY (`id_room_course`) REFERENCES `room_course` (`id_room_course`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_evaluation_type_evaluation1` FOREIGN KEY (`id_type_evaluation`) REFERENCES `type_evaluation` (`id_type_evaluation`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `eva_std_rom_crs`
--
ALTER TABLE `eva_std_rom_crs`
  ADD CONSTRAINT `fk_eva_std_rom_crs_evaluation1` FOREIGN KEY (`id_evaluation`) REFERENCES `evaluation` (`id_evaluation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_eva_std_rom_crs_std_rom_crs1` FOREIGN KEY (`id_std_rom_crs`) REFERENCES `std_rom_crs` (`id_std_rom_crs`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grade`
--
ALTER TABLE `grade`
  ADD CONSTRAINT `fk_grade_level1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `option_profile`
--
ALTER TABLE `option_profile`
  ADD CONSTRAINT `fk_option_profile_option1` FOREIGN KEY (`id_option`) REFERENCES `option` (`id_option`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_option_profile_profile1` FOREIGN KEY (`id_profile`) REFERENCES `profile` (`id_profile`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `parent`
--
ALTER TABLE `parent`
  ADD CONSTRAINT `fk_parent_person1` FOREIGN KEY (`id_parent`) REFERENCES `person` (`id_person`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `fk_student_parent1` FOREIGN KEY (`id_parent`) REFERENCES `parent` (`id_parent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_student_person1` FOREIGN KEY (`id_student`) REFERENCES `person` (`id_person`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_student_grade_section1` FOREIGN KEY (`id_room`) REFERENCES `room` (`id_room`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `fk_teacher_person1` FOREIGN KEY (`id_teacher`) REFERENCES `person` (`id_person`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_person1` FOREIGN KEY (`id_person`) REFERENCES `person` (`id_person`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_profile1` FOREIGN KEY (`id_profile`) REFERENCES `profile` (`id_profile`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
